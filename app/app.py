from flask import Flask,render_template,request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
       user = {'Fin': request.args.get('optionsRadiosFin'), 'Padre': request.args.get('optionsRadiosPadres')}
       return render_template('index.html', title='GET', user=user)
    else:
       user = {'Fin': request.form['optionsRadiosFin'], 'Padre': request.form['optionsRadiosPadres']}
       return render_template('index.html', title='POST', user=user)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')