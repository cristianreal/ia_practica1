import random
from random import randrange

CantPoblacion = 4
TipoCriterio = 0

def LeerCSV(ruta):
    return [[75, 50, 90, 65],[80, 95, 88, 80],[20, 55, 60, 58],[60, 28, 69, 50]]

def Poblacion():
    poblacion = []
    for x in range(CantPoblacion):
        individuo = []
        for y in range(3):
            individuo.append(round(random.uniform(-1, 1), 2))
        poblacion.append(individuo)
    return poblacion

def Criterio(tcriterio, poblacion):
    result = None
    if tcriterio == 1: #Fitness
        individuofit = []
        for individuo in poblacion:
            individuofit.append({'Val': Evaluacion(individuo), 'Ind':individuo})
        individuofit.sort(key = (lambda e: e['Val']))
        return individuofit[0].pop('Ind')
    elif tcriterio == 2: #Fitness promedio
        return result
    elif tcriterio == 3: #Generacion
        return result
    else:
        print("No selecciono tipo de criterio")
        return result

def Evaluacion(individuo):
    ErrorCuadratico = 0
    tuplaClase = LeerCSV("ruta")
    for x in range(CantPoblacion - 1):
        tuplaEstudiante = tuplaClase[random.randint(0, len(tuplaClase) - 1)]
        p1 = tuplaEstudiante[0]
        p2 = tuplaEstudiante[1]
        p3 = tuplaEstudiante[2]
        w1 = individuo[0]
        w2 = individuo[1]
        w3 = individuo[2]
        NC = (w1*p1) + (w2*p2) +(w3*p3)
        NR = tuplaEstudiante[3]
        ErrorCuadratico += (NR - NC)**2
    ErrorCuadratico = ErrorCuadratico * 0.25
    return ErrorCuadratico

def Seleccionar(tseleccion, poblacion):
    result = []
    if tseleccion == 1: #mejor fitness ordenado
        solucion_ordenada = []
        for individuo in poblacion:
            solucion_ordenada.append({'Val': Evaluacion(individuo), 'Ind': individuo})
        solucion_ordenada.sort(reverse=True, key= (lambda e : e['Val']))
        contador = 0
        while contador < 5:
            result.append(solucion_ordenada[contador].pop('Ind'))
            contador += 1
        return result
    elif tseleccion == 2: #random
        return result
    elif tseleccion == 3: #top sin ordenar
        return result
    else:
        print("No selecciono tipo de seleccionar")
        return result

def Emparejar(Top5):
    newP = Top5
    newP.append(Mutar(Cruzar(Top5[0], Top5[1]))) #Hijo1
    newP.append(Mutar(Cruzar(Top5[2], Top5[4]))) #Hijo2
    newP.append(Mutar(Cruzar(Top5[0], Top5[2]))) #Hijo3
    newP.append(Mutar(Cruzar(Top5[2], Top5[4]))) #Hijo4
    newP.append(Mutar(Cruzar(Top5[0], Top5[4]))) #Hijo5
    return newP

def Cruzar(Padre1, Padre2):
    hijo = []
    for x in range(3):
        prob_cruzar = 0.5
        if random.random() >= prob_cruzar:
            hijo.append(Padre1[x])
        else:
            hijo.append(Padre2[x])
    return hijo

def Mutar(Hijo):
    posicion_aleatoria = random.randint(0, 2)
    Hijo[posicion_aleatoria] = Hijo[posicion_aleatoria] + round(random.uniform(-0.3, 0.3), 2)
    return Hijo

generacion = 0
p = Poblacion()
fin = Criterio(1, p)
while(fin == None):
    padres = Seleccionar(1, p)
    p = Emparejar(padres)
    fin = Criterio(1, p)
    generacion += 1

print(fin)
print(generacion)